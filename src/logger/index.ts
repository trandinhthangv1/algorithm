import { Item } from '../interfaces';

function logger(arr: Item[], total: number, enough: boolean) {
  arr.forEach((element) => {
    console.log(`[Contract with] ${element.name} ${element.container} container, price: ${element.totalCost}`);
  });
  if (!enough) console.log('Not enough containers');
  console.log(`[Summary] total cost ${total}`);
}

export default logger;
