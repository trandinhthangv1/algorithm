interface Item {
  name: string;
  container: number;
  totalCost: number;
}

interface Result {
  arr: Item[];
  total: null | number;
  enough: boolean;
}

export { Item, Result };
