import { Item } from '../interfaces';

const calcTotal = (arr: Item[]) => {
  return arr.reduce((previousValue, currentValue) => previousValue + currentValue.totalCost, 0);
};

export default calcTotal;
