import data from './data';
import Algorithm from './Algorithm';
import logger from './logger';

const bootstrap = () => {
  const neededContainer = data[2].neededContainer;
  const listings = data[2].listings;

  const algorithm = new Algorithm();

  const result = algorithm.findLowestPrice(listings, neededContainer);

  logger(result.arr, result.total as number, result.enough);
};

bootstrap();
