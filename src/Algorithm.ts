import { Item, Result } from './interfaces';
import calcTotal from './utils/calcTotal';

class Algorithm {
  private neededContainer: number;
  private listings: Item[];
  private result: Result = {
    arr: [],
    total: null,
    enough: false,
  };

  findLowestPrice(listings: Item[], neededContainer: number) {
    const TOTAL_CONTAINER_INIT = 0;
    const INDEX_INIT = 0;
    this.neededContainer = neededContainer;
    this.listings = listings;

    this.find(TOTAL_CONTAINER_INIT, INDEX_INIT, []);

    if (!this.result.enough) {
      const total = calcTotal(this.listings);
      this.result.arr = this.listings;
      this.result.total = total;
    }

    return this.result;
  }

  private find(totalContainer: number, index: number, arr: Item[]) {
    if (totalContainer == this.neededContainer) {
      const total = calcTotal([...arr]);

      if (this.result.total === null || (this.result.total && total < this.result.total)) {
        this.result.arr = [...arr];
        this.result.total = total;
      }

      this.result.enough = true;

      return;
    }

    for (let i = index; i < this.listings.length; i++) {
      if (i != index && this.listings[i].container == this.listings[i - 1].container) continue;

      if (totalContainer > this.neededContainer) return;

      arr.push(this.listings[i]);
      this.find(totalContainer + this.listings[i].container, i + 1, arr);
      arr.pop();
    }
  }
}

export default Algorithm;
